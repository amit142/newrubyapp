ARG RUBY_VERSION=3.2.2
FROM registry.docker.com/library/ruby:$RUBY_VERSION-slim as base

WORKDIR /ruby_app

ENV RAILS_ENV="production" \
    BUNDLE_DEPLOYMENT="1" \
    BUNDLE_PATH="/usr/local/bundle" \
    BUNDLE_WITHOUT="development"


FROM base as build

RUN apt-get update -qq && \
    apt-get install --no-install-recommends -y build-essential git libvips pkg-config

COPY ./Gemfile Gemfile.lock ./
RUN bundle install && \
    rm -rf ~/.bundle/ "${BUNDLE_PATH}"/ruby/*/cache "${BUNDLE_PATH}"/ruby/*/bundler/gems/*/.git && \
    bundle exec bootsnap precompile --gemfile

COPY . .

RUN bundle exec bootsnap precompile app/ lib/

ENV RAILS_MASTER_KEY="e6a128a50b30fe775eb2bbfe37fb1e51"

RUN SECRET_KEY_BASE_DUMMY=1 ./bin/rails assets:precompile


FROM base

RUN apt-get update -qq && \
    apt-get install --no-install-recommends -y curl libsqlite3-0 libvips && \
    rm -rf /var/lib/apt/lists /var/cache/apt/archives

COPY --from=build /usr/local/bundle /usr/local/bundle
COPY --from=build /ruby_app /ruby_app

EXPOSE 3000

ENV SECRET_KEY_BASE="dgvfdfhvbdhfbdjhbdjhfb"
ENV RAILS_MASTER_KEY="e6a128a50b30fe775eb2bbfe37fb1e51"

# {% for key in env_vars %}
# ENV {{key}}="{{env_vars[key]}}"
# {% endfor %}

RUN bin/rails db:migrate

CMD ["bin/rails", "server", "-b", "0.0.0.0"]
